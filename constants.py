import pandas as pd

region_info = pd.DataFrame({'LDZ': {0: 'EA', 1: 'EM', 2: 'LC', 3: 'LO', 4: 'LS', 5: 'LT', 6: 'NE', 7: 'NO',
                                    8: 'NT', 9: 'NW', 10: 'SC', 11: 'SE', 12: 'SO', 13: 'SW', 14: 'WM', 15: 'WN',
                                    16: 'WS'},
                            'Greater Region': {0: 'EoE', 1: 'EoE', 2: 'SC', 3: 'SC', 4: 'SC', 5: 'SC', 6: 'N', 7: 'N',
                                               8: 'Lo', 9: 'NW', 10: 'SC', 11: 'S', 12: 'S', 13: 'WW', 14: 'WM',
                                               15: 'WW', 16: 'WW'},
                            'Company': {0: 'National Grid Gas', 1: 'National Grid Gas', 2: 'Scotland', 3: 'Scotland',
                                        4: 'Scotland', 5: 'Scotland', 6: 'Northern Gas Networks Ltd',
                                        7: 'Northern Gas Networks Ltd', 8: 'National Grid Gas', 9: 'National Grid Gas',
                                        10: 'Scotland', 11: 'Southern Gas Networks Ltd',
                                        12: 'Southern Gas Networks Ltd', 13: 'Wales & West Utilities',
                                        14: 'National Grid Gas', 15: 'Wales & West Utilities',
                                        16: 'Wales & West Utilities'}})
