import datetime
import pandas as pd
from sqltools import sql_connect
from calendar import monthrange

engine = sql_connect(server='vms-pnd-001')


def get_commodity_cust_info(datetext,
                            date):  # Gets data needed from Gas AFMS and Gas forecasting tables for commodity caculations.
    today = datetime.datetime.today()
    if date < datetime.datetime(today.year, today.month - 1, 1):
        meter_status_codes = "('3','9','22','30','31')"
    else:
        meter_status_codes = "('3','9','22','27','30','31')"
    commodity_cust_info_query = f"""SET NOCOUNT ON;
    DECLARE @DATEVAR varchar(12)
    
    Set @DATEVAR = '{datetext}'
    --DECLARE @TSQL varchar(8000) --, @DATEVAR char(11)
    DECLARE @YEAR char(2)
    SET @YEAR = right(@datevar,2)
    
    -- Get data from AFMS - Euccode is derived but probably no longer needed
    IF OBJECT_ID('tempdb..#AFMSTAB') IS NOT NULL 	 DROP TABLE #AFMSTAB	
    IF OBJECT_ID('tempdb..#workingtab') IS NOT NULL 	 DROP TABLE #workingtab	
    CREATE TABLE #AFMSTab (MPRN varchar(13), SupplyStartDate date,SupplyEndDate date,LDZ varchar(10), Exit_Zone 
    varchar(10), AQ varchar(10),EUC2 varchar(10),EUC varchar(10),SOQ varchar(10),IsImp varchar(10),status varchar(
    10),k0543 varchar(10),status_code_fk varchar(10)) 
    INSERT INTO #AFMSTab
    SELECT
    
         a.[K0533] as MPRN
        ,a.[K0116] as 'SupplyStartDate'
        ,a.[K0882] as 'SupplyEndDate'
        ,a.[K0494] as LDZ
        ,a.[K0249] as Exit_Zone
        ,a.[K0530] as AQ
        ,a.K0494 + ':E' + cast('  + @YEAR + ' as char(2)) + 
            case when (k0530  <= 73200) THEN '01B' 
                 when (k0530 > 73200   and k0530 <= 293000 ) THEN '02B'
                 when (K0530 > 293000  and k0530 <= 732000 ) THEN '03B'
                 when (K0530 > 732000  and k0530 <= 2196000 ) THEN '04B'
                 when (K0530 > 2196000 and k0530 <= 5860000 ) THEN '05B'
                 when (K0530 > 5860000 and k0530 <= 14650000 ) THEN '06B'
                 when (K0530 > 14650000 and k0530 <= 29300000 ) THEN '07B'
                 when (K0530 > 29300000 and k0530 <= 58600000 ) THEN '08B'
                 when (K0530 > 58600000 and k0530 <= 58600000 ) THEN '09B'
                 END  as EUC2
        ,K0233 as EUC
        ,K0574 as SOQ
        ,[K0999] as 'IsImp'
        ,[K0546] as Status
        ,k0543
        ,status_code_fk
    FROM (
        SELECT a.k0533,a.k0116,a.k0882,a.k0494, a.k0249, b.k0530,c.k0233,a.k0574,b.k0999,b.k0546,b.k0543,a.status_code_fk 
        FROM [uh-gendb-01].[afmslocal].gas.combined_mprn a 
        INNER JOIN [uh-gendb-01].afmslocal.gas.combined_meter b on   b.MPRN_FK = a.MPRN_PK
        LEFT JOIN [uh-gendb-01].afmslocal.gas.combined_customer c on c.MPRN_FK = a.MPRN_PK
        WHERE 
        --K0116 <= getdate() -- Supply Start
        (a.K0882 is Null or (a.K0882 > dateadd(day,-1, @DATEVAR)) and a.K0882 > a.K0116) -- Supply End Date
    
        AND (K0543 > dateadd(day,-1, @DATEVAR) or K0543 is Null)  -- Meter removal date
        AND (K0546 = 'LI' or K0546 is Null) -- Meter Status
        --AND (K0533 <= 7400000000 OR K0533 >= 8000000000)  -- enable if IGTs not required
        AND status_code_fk   not in {meter_status_codes}
        AND k0116 <= @DATEVAR
        AND Shipper = 'REN'
        ) a
    
    
    
    CREATE TABLE #workingTab 
        (MPRN varchar(13), EffDate date,AQ int,EUCNo varchar(15),EUCcode varchar(50),sourceLoc varchar(3),
        effdate_nrl date,effdate_trf date, LDZ varchar(10), Exit_Zone varchar(10))
    
    INSERT INTO #workingTab
    SELECT  a.mprn, 
        CASE WHEN a.effdate_nrl > a.k0116_trf 
            THEN effdate_nrl 
            ELSE k0116_trf
        END AS EffDate,
        CASE WHEN a.effdate_nrl > a.k0116_trf 
            THEN AQ_NRL 
            ELSE AQ_TRF 
        END AS AQ,
        CASE WHEN a.effdate_nrl > a.k0116_trf 
            THEN 
                (SELECT euccode FROM Gas_Forecasting.dbo.EUCCodesbyYear WHERE a.euc_nrl = CAST(eucnum AS INT))  
            ELSE 
                (SELECT euccode FROM Gas_Forecasting.dbo.EUCCodesbyYear WHERE a.euc_trf15 = CAST(eucnum AS INT)) 
        END  AS EUCNo,
        CASE WHEN a.effdate_nrl > a.k0116_trf AND a.effdate_nrl IS NOT NULL 
            THEN a.euc_nrl   
            ELSE a.euc_trf15 
        END AS Euccode,
        CASE WHEN a.effdate_nrl > a.k0116_trf 
            THEN 'NRL' 
            ELSE 'TRF' 
        END,
        a.effdate_nrl,
        a.k0116_trf,
        a.LDZ,
        a.Exit_Zone
    FROM
    (
        SELECT t.mprn, ISNULL(n.k0116_Nrl,'01-JAN-2000') AS k0116_Nrl,ISNULL(trf.k0116_trf,'01-JAN-2000') AS k0116_trf, 
            n.Effdate_nrl,euc_nrl,euc_trf15,AQ_NRL,AQ_TRF,k0533_s92,MPRN_TRF,trf15.k0119_s15, t.LDZ, t.Exit_Zone
        FROM #AFMSTab t 
        LEFT JOIN 
    
        (SELECT k0533 AS k0533_s92 , COALESCE(K0821,k0530) AQ_NRL ,k0531 EffDate_NRL,k4149 AS SOQ_NRL, k0116 AS k0116_NRL,k0233 AS EUC_nrl 
            FROM Gas_Forecasting.dbo.NRL_T04 n
            WHERE k0531 = (SELECT  MAX(k0531) FROM Gas_Forecasting.dbo.NRL_T04 WHERE K0533 = n.k0533)
         UNION -- gmh - 26/9/2017 - Added the following in to allow for IGTs without a K0531 - next best in K4143 - not ideal
         SELECT k0533 AS k0533_s92 , COALESCE(K0821,k0530) AQ_NRL ,k4143 AS EffDate_NRL,k4149 AS SOQ_NRL, k0116 AS k0116_NRL,k0233 AS EUC_nrl 
            FROM Gas_Forecasting.dbo.NRL_T04 n
            WHERE k4143 = (SELECT MAX(k4143) FROM Gas_Forecasting.dbo.NRL_T04 WHERE K0533 = n.k0533 AND K0531 IS NULL))n
        ON t.mprn = n.K0533_s92
    
        LEFT JOIN 
        (SELECT k0533_s75 MPRN_TRF, k0530 AQ_TRF,k0116 AS k0116_trf,k0119_S15  
            FROM Gas_Forecasting.dbo.TRF_S75 tr
            WHERE k0116 = (SELECT  MAX(k0116) FROM Gas_Forecasting.dbo.TRF_S75 WHERE K0533_S75 = tr.k0533_s75)) trf
            ON t.mprn = trf.mprn_trf
        LEFT JOIN
        (SELECT k0119_s15 AS k0119_S15,k0700,k0699,k0494,k0233 AS EUC_trf15,k0249,k0062_s70 
            FROM Gas_Forecasting.dbo.Trf_S15 ) trf15
        ON trf.k0119_s15 = trf15.k0119_s15 
    
    ) a
    
    
    --Gas year starts in Oct - if euccode is from last year, correct it
    
    UPDATE #workingTab SET
    
    
    eucno =  ( CASE WHEN 10 - MONTH(@datevar) > 0 
                    THEN LEFT(eucno,4) + CAST((CAST(RIGHT(YEAR(@datevar),2)AS INT) - 1) AS VARCHAR(2))  + SUBSTRING(eucno,7,LEN(eucno))
                ELSE LEFT(eucno,4) + CAST(CAST(RIGHT(YEAR(@datevar),2) AS INT) AS VARCHAR(2))  + SUBSTRING(eucno,7,LEN(eucno))
                END)
                FROM #workingtab  
     --from #workingTab where eucno not like '%:E14%'
    -- Remove dupes from #workingTab
      ;WITH Dupes AS
     (SELECT 
        MPRN, effdate,aq,eucno,euccode,sourceloc,effdate_nrl,effdate_trf,
        ROW_NUMBER() OVER(PARTITION BY mprn ORDER BY mprn,effdate_nrl DESC) AS RowNum
      FROM  #workingTab )
     DELETE FROM Dupes WHERE RowNum > 1 
    
    -- Write out all EUC codes - 
     SELECT * FROM #workingTab ORDER BY mprn
     
     
    DROP TABLE #AFMSTab
    DROP TABLE #workingTab;
    
    SET NOCOUNT OFF"""

    return pd.read_sql(sql=commodity_cust_info_query, con=engine)


def get_capacity_cust_info(datetext,
                           date):  # Gets data needed from Gas AFMS and Gas forecasting tables for commodity caculations.
    today = datetime.datetime.today()
    if date < datetime.datetime(today.year, today.month - 1, 1):
        meter_status_codes = "('3','9','22','30','31')"
    else:
        meter_status_codes = "('3','9','22','25','27','30','31')"
    capacity_cust_info_query = f"""SET NOCOUNT ON;
DECLARE @DATEVAR varchar(12)

Set @DATEVAR = '{datetext}'

-- GMH written 2014
-- 11-Sep-2017 - Change pulling of data from AFMSlocal and updated for Nexus NRL and TRF flows
-- gmh -04-Oct-2017 - Added additional AFMS status codes and only pull mprns which are signified as shipper = 'REN'
-- 06-Feb-2018  Additional columns EffDate_FAQ FAQ FSOQ and SOQ  (copy of SP Gas_Current_sp)

--DECLARE @DATEVAR datetime = '01-Feb-2018'
DECLARE @TSQL varchar(8000) 
DECLARE @YEAR char(2) = right(@datevar,2)

-- Get data from AFMS - Euccode is derived but probably no longer needed
IF OBJECT_ID('tempdb..#AFMSTAB') IS NOT NULL    DROP TABLE #AFMSTAB 
IF OBJECT_ID('tempdb..#workingtab') IS NOT NULL	DROP TABLE #workingtab
    
CREATE TABLE #AFMSTab (MPRN varchar(13), SupplyStartDate date,SupplyEndDate date,LDZ varchar(10),Exit_Zone varchar(10) ,AQ varchar(10),EUC2 varchar(10),EUC varchar(10),SOQ varchar(10),IsImp varchar(10),status varchar(10),k0543 varchar(10),status_code_fk varchar(10))
INSERT INTO #AFMSTab
SELECT

       a.[K0533] as MPRN
       ,a.[K0116] as 'SupplyStartDate'
       ,a.[K0882] as 'SupplyEndDate'
       ,a.[K0494] as LDZ
	   ,a.[K0249] as Exit_Zone
       ,a.[K0530] as AQ
       ,a.K0494 + ':E' + cast('  + @YEAR + ' as char(2)) + 
        case when (k0530  <= 73200) THEN '01B' 
             when (k0530 > 73200   and k0530 <= 293000 ) THEN '02B'
             when (K0530 > 293000  and k0530 <= 732000 ) THEN '03B'
             when (K0530 > 732000  and k0530 <= 2196000 ) THEN '04B'
             when (K0530 > 2196000 and k0530 <= 5860000 ) THEN '05B'
             when (K0530 > 5860000 and k0530 <= 14650000 ) THEN '06B'
             when (K0530 > 14650000 and k0530 <= 29300000 ) THEN '07B'
             when (K0530 > 29300000 and k0530 <= 58600000 ) THEN '08B'
             when (K0530 > 58600000 and k0530 <= 58600000 ) THEN '09B'
                     END  as EUC2
       ,K0233 as EUC
       ,K0574 as SOQ
       ,[K0999] as 'IsImp'
       ,[K0546] as Status
       ,k0543
       ,status_code_fk
FROM (
       SELECT a.k0533,a.k0116,a.k0882,a.k0494,a.[K0249],b.k0530,c.k0233,a.k0574,b.k0999,b.k0546,b.k0543,a.status_code_fk 
       FROM [uh-gendb-01].[afmslocal].gas.combined_mprn a 
       INNER JOIN [uh-gendb-01].afmslocal.gas.combined_meter b on   b.MPRN_FK = a.MPRN_PK
       LEFT JOIN [uh-gendb-01].afmslocal.gas.combined_customer c on c.MPRN_FK = a.MPRN_PK
       WHERE 
       --K0116 <= getdate() -- Supply Start
       (a.K0882 is Null or (a.K0882 > dateadd(day,-1, @DATEVAR)) and a.K0882 > a.K0116) -- Supply End Date
       
       AND (K0543 > dateadd(day,-1, @DATEVAR) or K0543 is Null)  -- Meter removal date
       AND (K0546 = 'LI' or K0546 is Null) -- Meter Status
       --AND (K0533 <= 7400000000 OR K0533 >= 8000000000)  -- enable if IGTs not required
       AND status_code_fk   not in {meter_status_codes} -- removed 25, and 27 (to work for backdated - MPRNS that 
       --have left but were on supply) 
       AND k0116 <= @DATEVAR
       AND Shipper = 'REN'
       ) a

CREATE TABLE #workingTab 
       (MPRN varchar(13), EffDate_FAQ DATE, FAQ INT, FSOQ INT, 
	   EUCNo varchar(15),EUCcode varchar(50),sourceLoc varchar(3),
	   effdate_nrl date,effdate_trf date,
	   EffDate_AQ DATE, AQ INT, SOQ INT, LDZ varchar(10), Exit_Zone varchar(10))
 
INSERT INTO #workingTab (MPRN, EffDate_FAQ, FAQ, FSOQ, EUCNo, EUCcode, sourceLoc, effdate_nrl, effdate_trf, EffDate_AQ, AQ, SOQ, LDZ, Exit_Zone)
SELECT  a.mprn, 
        a.EffDate_FAQ,
	   CASE WHEN a.effdate_nrl > a.k0116_trf 
              THEN nrl_faq 
              ELSE trf_faq 
       END AS faq,
	   
	    CASE WHEN a.effdate_nrl > a.k0116_trf 
              THEN nrl_fsoq 
              ELSE trf_fsoq 
       END AS fsoq,
	   
	   CASE WHEN a.effdate_nrl > a.k0116_trf 
              THEN 
                     (SELECT euccode FROM Gas_Forecasting.dbo.EUCCodesbyYear WHERE a.euc_nrl = CAST(eucnum AS INT))  
              ELSE 
                     (SELECT euccode FROM Gas_Forecasting.dbo.EUCCodesbyYear WHERE a.euc_trf15 = CAST(eucnum AS INT)) 
       END  AS EUCNo,
       
	   CASE WHEN a.effdate_nrl > a.k0116_trf AND a.effdate_nrl IS NOT NULL 
              THEN a.euc_nrl   
              ELSE a.euc_trf15 
       END AS Euccode,
       
	   CASE WHEN a.effdate_nrl > a.k0116_trf 
              THEN 'NRL' 
              ELSE 'TRF' 
       END AS sourceLoc,
      a.effdate_nrl,
      a.k0116_trf,
	   	   
	   CASE WHEN a.effdate_nrl > a.k0116_trf 
              THEN effdate_nrl 
              ELSE k0116_trf
       END AS EffDate_AQ,   

	   CASE WHEN a.effdate_nrl > a.k0116_trf 
              THEN AQ_NRL 
              ELSE AQ_TRF 
       END AS AQ,       
      
       CASE WHEN a.effdate_nrl > a.k0116_trf 
              THEN nrl_soq 
              ELSE trf_soq 
       END AS soq,
	   LDZ,
       Exit_Zone

FROM
(
       SELECT t.mprn, ISNULL(n.k0116_Nrl,'01-JAN-2000') AS k0116_Nrl,ISNULL(trf.k0116_trf,'01-JAN-2000') AS k0116_trf, 
              n.Effdate_nrl,euc_nrl,euc_trf15,AQ_NRL,AQ_TRF,k0533_s92,MPRN_TRF,trf15.k0119_s15,nrl_fsoq,nrl_faq,nrl_soq,trf_fsoq,trf_faq,trf_soq, EffDate_FAQ, LDZ, Exit_Zone
       FROM #AFMSTab t 
       LEFT JOIN 

       (SELECT k0533 AS k0533_s92 , COALESCE(K0821,k0530) AQ_NRL ,k0531 EffDate_NRL,k4149 AS SOQ_NRL, k0116 AS k0116_NRL,k0233 AS EUC_nrl ,k4149 AS nrl_fsoq,k4148 AS nrl_faq, k4219 AS nrl_soq, k4232 AS EffDate_FAQ
              FROM Gas_Forecasting.dbo.NRL_T04 n
              WHERE k0531 = (SELECT  MAX(k0531) FROM Gas_Forecasting.dbo.NRL_T04 WHERE K0533 = n.k0533)
       UNION -- gmh - 26/9/2017 - Added the following in to allow for IGTs without a K0531 - next best in K4143 - not ideal
       SELECT k0533 AS k0533_s92 , COALESCE(K0821,k0530) AQ_NRL ,k4143 AS EffDate_NRL,k4149 AS SOQ_NRL, k0116 AS k0116_NRL,k0233 AS EUC_nrl ,k4149 AS nrl_fsoq,k4148 AS nrl_faq, k4219 AS nrl_soq, k4232 AS EffDate_FAQ
              FROM Gas_Forecasting.dbo.NRL_T04 n
              WHERE k4143 = (SELECT MAX(k4143) FROM Gas_Forecasting.dbo.NRL_T04 WHERE K0533 = n.k0533 AND K0531 IS NULL))n
       ON t.mprn = n.K0533_s92

       LEFT JOIN 
       (SELECT k0533_s75 MPRN_TRF, k0530 AQ_TRF,k0116 AS k0116_trf,k0119_S15  
              FROM Gas_Forecasting.dbo.TRF_S75 tr
              WHERE k0116 = (SELECT  MAX(k0116) FROM Gas_Forecasting.dbo.TRF_S75 WHERE K0533_S75 = tr.k0533_s75)) trf
              ON t.mprn = trf.mprn_trf
       LEFT JOIN																					-- removed k4148
       (SELECT k0119_s15 AS k0119_S15,k0700,k0699,k0494,k0233 AS EUC_trf15,k0249,k0062_s70,k4149 AS trf_fsoq, NULL AS trf_faq, NULL AS trf_soq
              FROM Gas_Forecasting.dbo.Trf_S15 ) trf15
       ON trf.k0119_s15 = trf15.k0119_s15 

) a

--Gas year starts in Oct - if euccode is from last year, correct it

UPDATE #workingTab SET

eucno =  ( CASE WHEN 10 - MONTH(@datevar) > 0 
                           THEN LEFT(eucno,4) + CAST((CAST(RIGHT(YEAR(@datevar),2)AS INT) - 1) AS VARCHAR(2))  + SUBSTRING(eucno,7,LEN(eucno))
                     ELSE LEFT(eucno,4) + CAST(CAST(RIGHT(YEAR(@datevar),2) AS INT) AS VARCHAR(2))  + SUBSTRING(eucno,7,LEN(eucno))
                     END)
                     FROM #workingtab  
-- Remove dupes from #workingTab
  ;WITH Dupes AS
(SELECT 
    MPRN, EffDate_AQ, aq, eucno, euccode, sourceloc, effdate_nrl, effdate_trf,
       ROW_NUMBER() OVER(PARTITION BY mprn ORDER BY mprn,effdate_nrl DESC,effdate_faq DESC) AS RowNum
  FROM  #workingTab )
 DELETE FROM Dupes WHERE RowNum > 1 



-- Write out all EUC codes  
 SELECT MPRN, EffDate_FAQ, FAQ, FSOQ, EUCNo, EUCcode, sourceLoc, effdate_nrl, effdate_trf, EffDate_AQ, AQ, SOQ, LDZ, Exit_Zone
 FROM #workingTab 
 ORDER BY mprn

"""
    return pd.read_sql(sql=capacity_cust_info_query, con=engine)


def get_costs(date):
    costs_query = f"""SELECT LDZ, Network_Short_Code, Min_AQ, Max_AQ, Price, Cost_ID
      FROM [Pricing].[dbo].[Gas_Price_Inputs_LDZ]
      where Upload_Date = (Select MAX(Upload_Date) FROM [Pricing].[dbo].[Gas_Price_Inputs_LDZ]) 
      and From_Date <= '{date.strftime('%Y-%m-%d')}' and To_Date >= '{date.strftime('%Y-%m-%d')}'"""
    costs_query_gt_732000 = f"""SELECT LDZ, Network_Short_Code, Min_AQ, Minimum, Multiplier, Power_Factor, Cost_ID
      FROM [Pricing].[dbo].[Gas_Price_Inputs_LDZ_GT_732000]
      where Upload_Date = (Select MAX(Upload_Date) FROM [Pricing].[dbo].[Gas_Price_Inputs_LDZ_GT_732000]) 
      and From_Date <= '{date.strftime('%Y-%m-%d')}' and To_Date >= '{date.strftime('%Y-%m-%d')}'"""
    exit_costs_query = f"""SELECT Exit_Zone, Cost_ID, Price
    FROM [Pricing].[dbo].[Gas_Price_Inputs_LDZ_Exit]
        WHERE Upload_Date = (Select MAX(Upload_Date) FROM [Pricing].[dbo].[Gas_Price_Inputs_LDZ_Exit]) 
        AND From_Date <= '{date.strftime('%Y-%m-%d')}' AND To_Date >= '{date.strftime('%Y-%m-%d')}'"""
    nts_costs_query = f"""SELECT Cost_ID, Price
    FROM [Pricing].[dbo].[Gas_Price_Inputs_NTS]
        WHERE Upload_Date = (Select MAX(Upload_Date) FROM [Pricing].[dbo].[Gas_Price_Inputs_NTS]) 
        AND From_Date <= '{date.strftime('%Y-%m-%d')}' AND To_Date >= '{date.strftime('%Y-%m-%d')}'"""
    costs = add_scot_subregions_to_cost(pd.read_sql(sql=costs_query, con=engine))
    costs_gt_732000 = add_scot_subregions_to_cost(pd.read_sql(sql=costs_query_gt_732000, con=engine))
    exit_capacity_costs = pd.read_sql(sql=exit_costs_query, con=engine)
    nts_cost = sum(pd.read_sql(sql=nts_costs_query, con=engine).drop_duplicates()['Price'])

    return costs, costs_gt_732000, exit_capacity_costs, nts_cost


def add_scot_subregions_to_cost(costs):
    costs_scot = costs.loc[costs['LDZ'] == 'SC']
    for ldz in ['LC', 'LO', 'LS', 'LT', 'LW']:
        costs_scot['LDZ'] = ldz
        costs = costs.append(costs_scot)
    return costs


def get_demand(start_of_month_string, end_of_month_string):  # gets demand from pricing tables (originally from Gemini)
    query = f"SELECT Exit_Zone, LDZ, Meter_Type, SUM(Net_Allocations) as 'Net_Allocations' FROM [Pricing].[dbo].[Gas_Demand_Gemini_UG]  where Settlement_Date between '{start_of_month_string}' and '{end_of_month_string}' group by Exit_Zone, LDZ, Meter_Type"
    demand = pd.read_sql(sql=query, con=engine)
    demand = demand.pivot(index=['Exit_Zone','LDZ'], columns='Meter_Type', values='Net_Allocations').reset_index() #pivot to get column for UG and non-UG
    return demand


def get_load_factors(date):
    if date.month >= 10:
        gasyear = date.year + 1
    else:
        gasyear = date.year
    load_factors_query = f"""SELECT EucCode, LoadFactor
    FROM [Gas_Forecasting].[dbo].[EUCCodesbyYear] where GasYear = {str(gasyear)}"""
    return pd.read_sql(sql=load_factors_query, con=engine)
