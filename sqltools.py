import sqlalchemy
import urllib

# %%
__version__ = "0.0.3"


# %% FUNCTIONS
def create_pyodbc_conn_str(server, driver=None, trusted=True, database=None):
    """Create the pyodbc connection string to access the right server,
    database, ...

    Parameters
    ----------
    server : str
        Name of the server.
    driver: str, optional, default: None
        If None, the driver is set to '{SQL Server Native Client 11.0}'.
    trusted: bool, optional, default: True
        If True, it establishes a trusted connection.
    database: str, optional, default: None
        If not None, it accesses a specific databes in the server.

    Returns
    -------
    str
    """
    if driver is None:
        driver = '{SQL Server Native Client 11.0}'

    trusted_str = 'yes' if trusted else 'no'
    conn_str = f"Driver={driver};" \
               f"Server={server};" \
               f"Trusted_Connection={trusted_str};"

    if database is not None:
        conn_str += f"Database={database};"
    return conn_str


def sql_connect(server, **create_pyodbc_conn_str_kwargs):
    """Create a pyodbc connection.

    Parameters
    ----------
    server : str
        Name of the server it connects to.

    Returns
    -------
    sqlalchemy.Engine. Additional kwargs will be passed to the function
    create_pyodbc_conn_str.
    """
    # create connection str
    conn_str = create_pyodbc_conn_str(server=server,
                                      **create_pyodbc_conn_str_kwargs)
    params = urllib.parse.quote_plus(conn_str)
    # create engine
    engine = \
        sqlalchemy.create_engine(f"mssql+pyodbc:///?odbc_connect={params}")
    return engine
