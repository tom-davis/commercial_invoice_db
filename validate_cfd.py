import pandas as pd

from sqltools import sql_connect

engine = sql_connect(server='lh-etrmdb-01')


def toList(col):
    list = col.unique()
    return "(" + (', '.join('\'' + 'UKP' + item + '\'' for item in list)) + ")"


def validate_cfd(T001):
    settDates = toList(T001['/BIC/N1_J0073'].drop_duplicates().astype(str))
    query = """SELECT Period_Id, SUM(Volume)as 'Volume'
  FROM [entrader].[Entrader].[Position_Period]
  where Position_Id = 137
  and left(Period_Id,11) in """ + settDates + 'group by Period_Id'
    df = pd.read_sql(sql=query, con=engine)
    df['/BIC/N1_J0073'] = df['Period_Id'].str[3:11].astype(int)
    df['/BIC/N1_J0074'] = df['Period_Id'].str[-2:].astype(int)
    T001 = T001.merge(df, on=['/BIC/N1_J0073','/BIC/N1_J0074'])
    T001['Charge'] = T001['/BIC/N1_J1910']*T001['Volume']
    T001['Actual Charge'] = T001['/BIC/N1_J1910']*T001['/BIC/N1_J1911']
    T001.groupby(['/BIC/N1_J0073', '/BIC/N1_J1909']).agg({'/BIC/N1_J1907':'mean', '/BIC/N1_J1908':'mean', 'Charge':'sum', 'Actual Charge':'sum','Volume':'sum'})
    #Compare actual charge to charge and check previous settlement runs to work our net payment
    print(df)


validate_cfd(pd.read_csv("V:\\Trading\\Commercial\\Industry Costs\\CFD\\Backing Data\\Quarterly Reconciliations\\RENC_20210111161213_T004.csv"))
