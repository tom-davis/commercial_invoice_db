from calendar import monthrange
import datetime
import queries
import numpy as np
import pandas as pd
from constants import region_info


def calculate_commodity_costs(costs, costs_gt_732000, demand, cust_info, load_factors, nts_cost):
    cust_info['AQ_Category'] = cust_info['AQ'].apply(lambda
                                                         x: 0 if x < 73200 else 73200 if x < 732000 else 732000)  # create column that we can join to min_AQ from the cost tables

    cust_info = cust_info.merge(costs[['LDZ', 'Min_AQ', 'Price']], how='left', left_on=['LDZ', 'AQ_Category'],
                                right_on=['LDZ', 'Min_AQ'])  # join costs to customer info ready for calcs
    cust_info = cust_info.merge(costs_gt_732000[['LDZ', 'Min_AQ', 'Minimum', 'Multiplier', 'Power_Factor']],
                                how='left',
                                left_on=['LDZ', 'AQ_Category'], right_on=['LDZ', 'Min_AQ'])  # also join costs_gt_732000

    aq_sum = cust_info[['Exit_Zone', 'AQ']].groupby('Exit_Zone').sum().rename(columns={'AQ': 'AQ_Sum'})
    cust_info = cust_info.merge(aq_sum, how='left', on='Exit_Zone')
    cust_info['AQ_Proportion'] = cust_info['AQ'] / cust_info['AQ_Sum']

    cust_info = cust_info.merge(demand[['Exit_Zone', 'UG', 'NA']], how='left', on='Exit_Zone')
    cust_info['Total Demand'] = (cust_info['UG'] + cust_info['NA']) * cust_info['AQ_Proportion']
    cust_info['Non-UG Demand'] = cust_info['NA'] * cust_info['AQ_Proportion']

    # Get Load factors for SOQs
    cust_info = cust_info.merge(load_factors, how='left', left_on='EUCNo',
                                right_on='EucCode')
    cust_info['soq'] = cust_info['AQ'] / (365 * cust_info['LoadFactor'])  # calc soq

    cust_info.loc[cust_info['AQ_Category'] <= 73200, 'Cost'] = cust_info['Total Demand'] * cust_info[
        'Price'] / 100  # calculate cost for <732000
    cust_info.loc[cust_info['AQ_Category'] > 73200, 'Cost'] = cust_info['Total Demand'] * (
            cust_info['Multiplier'] * cust_info['soq'] ** cust_info[
        'Power_Factor']) / 100  # calculate cost for gt_732000
    cust_info['NTS_Cost'] = cust_info['Non-UG Demand'] * nts_cost
    cost_by_ldz = cust_info[['LDZ', 'Cost', 'NTS_Cost']].groupby('LDZ').sum()  # sum costs by ldz
    costs_by_company = cost_by_ldz.reset_index().merge(region_info[['LDZ', 'Company']], on='LDZ')[
        ['Company', 'Cost', 'NTS_Cost']].groupby('Company').sum()  # sum costs by company
    return costs_by_company


def calculate_capacity_costs(date, end_of_month, costs, exit_costs, costs_gt_732000, cust_info, load_factors):
    cust_info = cust_info.merge(load_factors, how='left', left_on='EUCNo',
                                right_on='EucCode')  # join load factors to cust_info

    cust_info['BestFormulaSOQ'] = cust_info.apply(lambda x: x['FSOQ'] if not np.isnan(x['FSOQ']) else x['SOQ']
    if not np.isnan(x['SOQ']) else x['FAQ'] / (365 * x['LoadFactor']) if not np.isnan(x['FAQ']) else x['AQ'] / (
            365 * x['LoadFactor']),
                                                  axis=1)  # create the best formula soq which takes the FSOQ if possible and otherwise takes the best piece of information
    cust_info['AQ_Category'] = cust_info['AQ'].apply(lambda
                                                         x: 0 if x < 73200 else 73200 if x < 732000 else 732000)  # create column that we can join to min_AQ from the cost tables
    cap_costs = costs.loc[costs['Cost_ID'] == 2]  # get capacity costs from all costs
    cust_cap_costs = costs.loc[costs['Cost_ID'] == 5]  # get customer capacity costs
    cust_cap_costs = cust_cap_costs.rename(columns={'Price': 'Customer_Price'})
    exit_costs = exit_costs.rename(columns={'Price': 'Exit_Price'})
    cap_costs = cap_costs.merge(cust_cap_costs, on=['LDZ', 'Min_AQ', 'Network_Short_Code',
                                                    'Max_AQ'])  # merge the two capacity costs together
    cap_costs_gt_732000 = costs_gt_732000.loc[costs_gt_732000['Cost_ID'] == 2]  # repeat for gt_732000 costs.
    cust_cap_costs_gt_732000 = costs_gt_732000.loc[costs_gt_732000['Cost_ID'] == 5]
    cust_cap_costs_gt_732000 = cust_cap_costs_gt_732000.rename(
        columns={'Minimum': 'Customer_Minimum', 'Multiplier': 'Customer_Multiplier',
                 'Power_Factor': 'Customer_Power_Factor'})
    cap_costs_gt_732000 = cap_costs_gt_732000.merge(cust_cap_costs_gt_732000,
                                                    on=['LDZ', 'Min_AQ', 'Network_Short_Code'])
    cust_info = cust_info.merge(cap_costs[['LDZ', 'Min_AQ', 'Price', 'Customer_Price']], how='left',
                                left_on=['LDZ', 'AQ_Category'],
                                right_on=['LDZ', 'Min_AQ'])  # merge costs to the customer info
    cust_info = cust_info.merge(cap_costs_gt_732000[
                                    ['LDZ', 'Min_AQ', 'Minimum', 'Multiplier', 'Power_Factor', 'Customer_Minimum',
                                     'Customer_Multiplier', 'Customer_Power_Factor']], how='left',
                                left_on=['LDZ', 'AQ_Category'],
                                right_on=['LDZ', 'Min_AQ'])  # merge gt_732000 costs to the customer info
    cust_info = pd.merge(cust_info, exit_costs, how='left', on="Exit_Zone")

    for cost_type in ['', 'Customer_']:  # same calculations for standard capacity and customer capacity
        cust_info.loc[cust_info['AQ_Category'] <= 73200, f'{cost_type}Capacity_Cost'] = cust_info['BestFormulaSOQ'] * \
                                                                                        cust_info[
                                                                                            f'{cost_type}Price'] / 100 * (
                                                                                                (
                                                                                                        end_of_month - date).days + 1)  # calculate capacity costs for <732000
        cust_info.loc[cust_info['AQ_Category'] > 73200, f'{cost_type}Capacity_Cost'] = cust_info['BestFormulaSOQ'] * (
                cust_info[f'{cost_type}Multiplier'] * cust_info['BestFormulaSOQ'] ** cust_info[
            f'{cost_type}Power_Factor']) / 100 * (
                                                                                               (
                                                                                                       end_of_month - date).days + 1)  # calculate capacity cost for gt_732000
    cust_info['ECN/C04 Cost'] = cust_info['BestFormulaSOQ'] * cust_info['Price'] / 100 * (
            (end_of_month - date).days + 1)  # create column that calculates exit cost for each mpan
    cap_cost_by_ldz = cust_info[['LDZ', 'Capacity_Cost', 'Customer_Capacity_Cost', 'ECN/C04 Cost']].groupby(
        'LDZ').sum()  # sum capacity costs by LDZ
    cap_costs_by_company = cap_cost_by_ldz.reset_index().merge(region_info[['LDZ', 'Company']], on='LDZ')[
        ['Company', 'Capacity_Cost', 'Customer_Capacity_Cost', 'ECN/C04 Cost']].groupby(
        'Company').sum()  # Sum capacity costs by Company
    return cap_costs_by_company


def calculate_fixed_costs(date, end_of_month, costs, cust_info):
    cust_info['AQ_Category'] = cust_info['AQ'].apply(lambda
                                                         x: 0 if x < 73200 else 73200 if x < 732000 else 732000)  # create a column to divide dataset into 3 AQ groups
    cust_info = cust_info.loc[
        cust_info['AQ_Category'] == 73200]  # filter to show only customers who's AQ is between 73200 and 732000
    cust_info = cust_info.merge(costs[['LDZ', 'Price']], how='left', on=['LDZ'])  # merge costs to customer info
    cust_info['fixed_cost'] = cust_info['Price'] / 100 * (
            (
                    end_of_month - date).days + 1)  # add a column that calculates the fixed cost for the month we are validating
    fixed_cost_by_ldz = cust_info[['LDZ', 'fixed_cost']].groupby('LDZ').sum()  # sum fixed costs by LDZ
    fixed_cost_by_company = fixed_cost_by_ldz.reset_index().merge(region_info[['LDZ', 'Company']], on='LDZ')[
        ['Company', 'fixed_cost']].groupby('Company').sum()  # sum fixed costs by company
    return fixed_cost_by_company


def calculate_gas_shipper_costs(date): #main function which calculates all the gas shipper costs
    end_of_month = datetime.datetime(date.year, date.month,
                                     monthrange(date.year, date.month)[1])  # get end of month date

    # Get start and end of month as strings
    end_of_month_string = end_of_month.strftime('%Y-%m-%d')
    start_of_month_string = date.strftime('%Y-%m-%d')

    costs, costs_gt_732000, exit_capacity_costs, nts_cost = queries.get_costs(
        date)  # Get Gas costs from pricing tables (capacity & commodity)
    demand = queries.get_demand(start_of_month_string, end_of_month_string)  # Get demand from Gemini

    commodity_cust_info = queries.get_commodity_cust_info(
        end_of_month_string, date)  # Get MPRNs and relevant customer data for commodity calculations
    capacity_cust_info = queries.get_capacity_cust_info(end_of_month_string,
                                                        date)  # Get MPRNs and relevant customer data for capacity calculations
    # potential overlap of sql code between getting the two queries above - we should explore combining into one

    load_factors = queries.get_load_factors(date)

    commodity_costs = calculate_commodity_costs(costs.loc[costs['Cost_ID'] == 1],
                                                costs_gt_732000.loc[costs_gt_732000['Cost_ID'] == 1],
                                                demand, commodity_cust_info, load_factors, nts_cost)
    capacity_costs = calculate_capacity_costs(date, end_of_month, costs, exit_capacity_costs, costs_gt_732000,
                                              capacity_cust_info, load_factors)
    fixed_costs = calculate_fixed_costs(date, end_of_month, costs.loc[costs['Cost_ID'] == 3], commodity_cust_info)
    return commodity_costs, capacity_costs, fixed_costs


date = datetime.datetime(2021, 2, 1)  # Start Date of Month we're validating
com_costs, cap_costs, fix_costs = calculate_gas_shipper_costs(date)
